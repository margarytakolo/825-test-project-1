#!/bin/bash

PROJECT_IDS=(34728847)

#Body of the merge request:
BODY='{
    "id": "placeholder_id",
    "source_branch": "develop",
    "target_branch": "main",
    "title": "Merge develop into master",
    "description": "Request generated automatically on weekly bases: merge develop into main",
    "draft": false,
    "squash": false,
    "should_remove_source_branch": false
}';

#For each project id, create a new merge request if does not exist:
for i in ${PROJECT_IDS[@]}; do

    #Get list of open merge requests from develop to main
    LIST_EXISTING_REQUESTS=`curl -X GET "https://gitlab.com/api/v4/projects/${i}/merge_requests?state=opened" --header "PRIVATE-TOKEN:${MERGE_REQUEST_TOKEN}"`;
    EXISTING_REQUESTS_DEVELOP_TO_MAIN=`echo ${LIST_EXISTING_REQUESTS} | grep -o '"source_branch":"develop"' | wc -l`;

    echo "creating MR for project with id "${i}"..."
    if [ ${EXISTING_REQUESTS_DEVELOP_TO_MAIN} -eq "0" ]; then
        BODY_WITH_PROJECT_ID=${BODY//'"placeholder_id"'/${i}};
        echo $BODY_WITH_PROJECT_ID
        curl -X POST "https://gitlab.com/api/v4/projects/${i}/merge_requests" --header "PRIVATE-TOKEN:${MERGE_REQUEST_TOKEN}" -H "Content-Type:application/json" -d "${BODY_WITH_PROJECT_ID}"
        
        curl -X PUT "https://gitlab.com/api/v4/projects/${i}/variables/TEST_VARIABLE" --header "PRIVATE-TOKEN:${MERGE_REQUEST_TOKEN}" --form "value=123"
        
        VARIABLE_VALUE=$(curl --header "PRIVATE-TOKEN:${MERGE_REQUEST_TOKEN}" "https://gitlab.com/api/v4/projects/${i}/variables/TEST_VARIABLE")
        CURRENT_RELEASE=$(jq -r '.value' $VARIABLE_VALUE)
        echo "updated variable":
        echo $CURRENT_RELEASE
    else
        echo "Merge request from develop to main already exists";
    fi
done 
